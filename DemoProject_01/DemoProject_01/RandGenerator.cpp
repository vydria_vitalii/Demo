﻿#include "RandGenerator.h"
#include <chrono>

std::mt19937 &RandGenerator::getMt19937()
{
	static RandGenerator rand;
	return rand.mMt;
}

RandGenerator::RandGenerator()
{
	std::random_device rd;
	if (rd.entropy())
	{
		std::seed_seq seed{ rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd() };
		mMt.seed(seed);
	}
	else
	{
		auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
		mMt.seed(static_cast<unsigned int>(seed));
	}
}