﻿#include "CThirdTask.h"
#include <algorithm>
#include <functional>
#include "function.h"

CThirdTask::CThirdTask(const char* const aName, const unsigned iMap, std::vector<int>& vector, std::map<int, int>& pairs)
	: CItem(aName, iMap),
	iVector(vector),
	iMap(pairs)
{
}

void CThirdTask::execute() const
{
	iVector.erase(std::remove_if(iVector.begin(), iVector.end(), [this](const int var)
	{
		return std::find_if(iMap.begin(), iMap.end(),
			[&](const std::pair<int, int> p) { return p.second == var; }
		) == iMap.end();
	}), iVector.end());
	for (auto&& it = begin(iMap); it != end(iMap); it = (end(iVector) == find(begin(iVector), end(iVector), it->second)) ? iMap.erase(it) : next(it));
	std::cerr << iVector << std::endl;
	std::cerr << iMap << std::endl;
}
