﻿#pragma once
#include <random>
#include "CItem.h"
#include "RandGenerator.h"

template<typename T, bool = std::is_base_of<std::uniform_int<>, T>::value
	|| std::is_base_of<std::uniform_real<>, T>::value>
class CAbstractRandItem;


template<typename T>
class CAbstractRandItem<T, true> abstract : public CItem
{
public:
	CAbstractRandItem(const char* aName, const unsigned int aId, const T &aUniform);
	CAbstractRandItem(const CAbstractRandItem& other) = delete;
	CAbstractRandItem(CAbstractRandItem&& other) noexcept = delete;
	CAbstractRandItem& operator=(const CAbstractRandItem& other) = delete;
	CAbstractRandItem& operator=(CAbstractRandItem&& other) noexcept = delete;
	auto rand() const
	{
		return iUniform(mMt);
	}
private:
	std::mt19937 &mMt;
	T iUniform;
};

template <typename T>
CAbstractRandItem<T, true>::CAbstractRandItem(const char* aName, const unsigned aId, const T &aUniform)
	: CItem(aName, aId), mMt(RandGenerator::getMt19937()), iUniform(std::move(aUniform))
{
}
