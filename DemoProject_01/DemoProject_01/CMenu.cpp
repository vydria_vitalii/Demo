﻿#include "CMenu.h"

void CMenu::addItem(const std::shared_ptr<CItem> aItem)
{
	if (!(std::find(iItems.begin(), iItems.end(), aItem) != iItems.end()))
	{
		iItems.push_back(aItem);
	}
}

CMenu::CMenu(CMenu&& other) noexcept: iItems(std::move(other.iItems))
{
}

CMenu& CMenu::operator=(CMenu&& other) noexcept
{
	if (this == &other)
		return *this;
	iItems = std::move(other.iItems);
	return *this;
}

const std::vector<std::shared_ptr<CItem>> &CMenu::getItems() const
{
	return this->iItems;
}
