﻿#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include "CMenu.h"
#include "CExecuteCommand.h"
#include "CFirsTask.h"
#include <iomanip>
#include "CSecondTask.h"
#include "CThirdTask.h"

using namespace  std;

int main(int argc, char *argv[])
{
	setlocale(LC_CTYPE, "rus");
	vector<int> v;
	map<int, int> m;
	shared_ptr<CMenu> menu = make_shared<CExecuteCommand>();
	menu->addItem(static_pointer_cast<CItem>(make_shared<CFirsTask>("заполнить случайными числами от 1 до 9 значения контейнеров",
		1, ref(v), ref(m))));
	menu->addItem(static_pointer_cast<CItem>(make_shared<CSecondTask>("удалить случайное число элементов (не более 15) в каждом контейнере",
		2, ref(v), ref(m))));
	menu->addItem(static_pointer_cast<CItem>(make_shared<CThirdTask>("после этого провести синхронизацию, чтобы в vector и map остались только имеющиеся в обоих контейнерах элементы (дубликаты не удалять).",
		3, v, m)));
	menu->execute();
	system("pause");
	return 0;
}