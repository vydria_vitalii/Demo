﻿#pragma once
#include "ICommand.h"
#include <sstream>      
class CItem abstract : public ICommand
{
public:
	CItem(const char *aName, const unsigned int);
	CItem(const CItem &aObj) = delete;
	CItem(CItem&& other) noexcept;
	CItem &operator = (const CItem& other) = delete;
	CItem &operator=(CItem &&other) noexcept;
	virtual ~CItem();
	const char* getName() const;
	unsigned int getId() const;
	virtual std::string toString() const;

private:
	char *iName;
	unsigned int iId;
	static void move(CItem &other);
};
