﻿#pragma once
#include "CMenu.h"

class CExecuteCommand : public CMenu
{
public:
	CExecuteCommand() = default;
	~CExecuteCommand() = default;
	CExecuteCommand(const CExecuteCommand &other) = delete;
	CExecuteCommand(CExecuteCommand &&other) noexcept = default;
	CExecuteCommand &operator=(const CExecuteCommand &other) = delete;
	CExecuteCommand &operator=(CExecuteCommand&& other) noexcept = default;
	void execute() const override;
};

