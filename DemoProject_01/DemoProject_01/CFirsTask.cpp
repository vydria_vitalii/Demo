﻿#include "CFirsTask.h"
#include <random>
#include <algorithm> 
#include <functional>
#include "function.h"

CFirsTask::CFirsTask(const char *aName, const unsigned int aId, std::vector<int>& aVector, std::map<int, int>& aMap)
	: CAbstractRandItem<std::uniform_int_distribution<int>>(aName, aId, std::uniform_int_distribution<int>(MIN_VALUE, MAX_VALUE)), iVector(aVector), iMap(aMap)
{
}

void CFirsTask::execute() const
{
	iVector.resize(COUNT);
	std::generate(iVector.begin(), iVector.end(), [this]()
	{
		return rand();
	});

	for (auto i = 0; i < COUNT; i++)
	{
		iMap.insert(std::make_pair(i, rand()));
	}

	std::cerr << iVector << std::endl;
	std::cerr << iMap << std::endl;
}
