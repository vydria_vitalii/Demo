﻿#pragma once
#include <vector>
#include <map>
#include "CAbstractRandItem.h"

class CSecondTask : public CAbstractRandItem<std::uniform_int_distribution<int>>
{
public:
	CSecondTask(const char* aName, const int aId, std::vector<int>& aVector, std::map<int, int>& aPairs);
	CSecondTask(const CSecondTask& other) = delete;
	CSecondTask(CSecondTask&& other) noexcept = delete;
	CSecondTask& operator=(const CSecondTask& other) = delete;
	CSecondTask& operator=(CSecondTask&& other) noexcept = delete;
	void execute() const override;
private:
	std::vector<int> &iVector;
	std::map<int, int> &iMap;
private:
	static constexpr const size_t MAX_RAND{ 15 };
};

