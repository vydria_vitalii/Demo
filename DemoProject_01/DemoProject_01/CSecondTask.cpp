﻿#include "CSecondTask.h"
#include "function.h"
CSecondTask::CSecondTask(const char* aName, const int aId, std::vector<int>& aVector, std::map<int, int>& aPairs)
	: CAbstractRandItem<std::uniform_int_distribution<int>>(aName, aId, std::uniform_int_distribution<int>(NULL, MAX_RAND)),
	iVector(aVector), iMap(aPairs)
{
}

void CSecondTask::execute() const
{
	iVector.erase(iVector.begin(), iVector.begin() + rand());
	iMap.erase(iMap.begin(), std::next(iMap.begin(), rand()));

	std::cerr << iVector << std::endl;
	std::cerr << iMap << std::endl;
}