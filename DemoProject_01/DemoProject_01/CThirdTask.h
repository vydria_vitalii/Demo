﻿#pragma once
#include "CItem.h"
#include <vector>
#include <map>

class CThirdTask : public CItem
{
public:
	CThirdTask(const char* const aName, const unsigned iMap, std::vector<int>& vector, std::map<int, int>& pairs);
	CThirdTask(const CThirdTask& other) = delete;
	CThirdTask(CThirdTask&& other) noexcept = delete;
	CThirdTask& operator=(const CThirdTask& other) = delete;
	CThirdTask& operator=(CThirdTask&& other) noexcept = delete;
	void execute() const override;
private:
	std::vector<int> &iVector;
	std::map<int, int> &iMap;
};
