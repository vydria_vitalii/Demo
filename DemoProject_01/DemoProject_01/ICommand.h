﻿#pragma once

class ICommand abstract 
{
public:
	ICommand() = default;
	ICommand(const ICommand &other) = delete;
	ICommand(ICommand &&other) noexcept = default;
	ICommand &operator=(const ICommand &other) = delete;
	ICommand &operator=(ICommand &&other) noexcept = default;
	virtual ~ICommand() = default;
	virtual void execute() const abstract;	
};
