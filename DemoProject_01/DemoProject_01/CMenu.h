﻿#pragma once
#include <vector>
#include "CItem.h"
#include <memory>

class CMenu abstract : public ICommand
{
public: 
	CMenu() = default;
	virtual void addItem(const std::shared_ptr<CItem> aItem);
	virtual ~CMenu() = default;
	CMenu(const CMenu &other) = delete;
	CMenu(CMenu&& other) noexcept;
	CMenu &operator=(const CMenu &other) = delete;
	CMenu &operator=(CMenu&& other) noexcept;
protected:
	virtual const std::vector<std::shared_ptr<CItem>> &getItems() const;
private:
	std::vector<std::shared_ptr<CItem>> iItems;
};
