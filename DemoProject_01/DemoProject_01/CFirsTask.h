﻿#pragma once
#include <vector>
#include <map>
#include "CAbstractRandItem.h"

class CFirsTask : public CAbstractRandItem<std::uniform_int_distribution<int>>
{
public:
	CFirsTask(const char *aName, const unsigned int aId, std::vector<int>& aVector, std::map<int, int>& aMap);
	CFirsTask(const CFirsTask& other) = delete;
	CFirsTask(CFirsTask&& other) noexcept = delete;
	CFirsTask& operator=(const CFirsTask& other) = delete;
	CFirsTask& operator=(CFirsTask&& other) noexcept = delete;
	~CFirsTask() = default;
	void execute() const override;
private:
	std::vector<int> &iVector;
	std::map<int, int> &iMap;
private:
	static constexpr const size_t MIN_VALUE{ 1 };
	static constexpr const size_t MAX_VALUE{ 9 };
	static constexpr const size_t COUNT{ 20 };
};
