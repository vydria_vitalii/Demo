﻿#pragma once
#include <map>
#include <vector>
#include <iostream>
#include <algorithm>

template<typename T>
std::ostream &operator << (std::ostream &os, std::vector<T> &v)
{
	os << "vector<" << typeid(T).name() << "> [";
	std::for_each(v.begin(), v.end(), [&os](const T &var) mutable
	{
		os << " " << var << " ";
	});
	os << "]";
	return os;
}

template <typename K, typename V>
std::ostream &operator << (std::ostream &os, std::map<K, V> &m)
{
	os << "map<" << typeid(K).name() << ", " << typeid(V).name() << "> [";
	for (auto &kv : m)
	{
		os << " " << kv.first << ":" << kv.second << " ";
	}
	os << "]";
	return os;
}
