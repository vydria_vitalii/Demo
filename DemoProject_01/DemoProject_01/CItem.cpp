﻿#include "CItem.h"
#include <utility>

CItem::CItem(const char* aName, const unsigned int aId) :
	iName(nullptr), iId(aId)
{
	size_t len;
	iName = new char[len = strlen(aName) + 1];
	strcpy_s(iName, len, aName);
}

CItem::CItem(CItem&& other) noexcept: ICommand(std::move(other)),
iName(std::move(other.iName)),
iId(std::move(other.iId))
{
	move(other);
}

CItem& CItem::operator=(CItem&& other) noexcept
{
	if (this == &other)
		return *this;
	iName = other.iName;
	iId = other.iId;
	move(other);
	return *this;
}

CItem::~CItem()
{
	if (iName)
	{
		delete[] iName;
	}
}

const char* CItem::getName() const
{
	return this->iName;
}

unsigned int CItem::getId() const
{
	return this->iId;
}

std::string CItem::toString() const
{
	std::ostringstream strStream;
	strStream << this->iId << " - " << this->getName();
	return strStream.str();
}

void CItem::move(CItem &other)
{
	other.iName = nullptr;
	other.iId = -1;
}
