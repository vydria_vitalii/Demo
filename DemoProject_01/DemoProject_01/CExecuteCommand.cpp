﻿#include "CExecuteCommand.h"
#include <iostream>

void CExecuteCommand::execute() const
{
	 for(const auto var : getItems())
	 {
		 std::cout << var->toString() << std::endl;
		 var->execute();
	 }
}
