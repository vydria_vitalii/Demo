﻿#pragma once
#include <random>

class RandGenerator final
{
public:
	static std::mt19937 &getMt19937();
	RandGenerator(const RandGenerator& other) = delete;
	RandGenerator(RandGenerator&& other) noexcept = delete;
	RandGenerator& operator=(const RandGenerator& other) = delete;
	RandGenerator& operator=(RandGenerator&& other) noexcept = delete;
private:
	RandGenerator();
	~RandGenerator() = default;
private:
	std::mt19937 mMt;
};
